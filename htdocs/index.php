
<html lang="en">

	<head>

		<meta charset="UTF-8">

		<title>A01_COLLERT</title>


	</head>

	<body>
		<h1 style= "text-align:center" >
			Assignment 01 - Collert, Amanda 
		<h1>
       		
		<h2>
			Summary
		</h2>
<div>
		<p>
        
            My pronouns are she/her. I am a third-year student, and I am majoring in Mathematics with a specialization in data science with a minor in computer science. I am hoping to graduate a semester early, December 2021 (fingers crossed). I love most foods, except for bananas and mushrooms.
          
		</p>

		<p>
       
			The love of my life is my dog. She is a three-year-old pit/boxer mix. She is a shy girl and does not like strangers at all, but she's a big lover when she knows you. Her favorite things are long walks and naps. She lived in Nevada with my sister for six months. 
        </p>
            <div>
            <img src="CJ.jpg" alt="CJ" width = "150" height = "200"/>
         	</div>
            Here is a picture of her.
		
			</div>
            
		<h2>
			Personal Information
		</h2>
        <div>
        
		<ul>
			
			<li>Name: Amanda Collert</li>
			<li>Address: 654 County Road 78, Mount Cory, OH 45868 </li>
			<li>Phone Number: 419-957-5816</li>
			<li>Email Address: collera@bgsu.edu</li>
			
		</ul>
        	</div>
            
		<h2>
        	Academic Information
        </h2>
        <div>
        <ul>
        	<li>High School: Cory-Rawson High School </li>
            	<ul>
                	<li>GPA: 3.992</li>
                	<li>Graduated 2nd in my class.</li>
                	<li>For my graduation speech, I read "one fish, two fish." </li>
            	</ul>
            <li>Undergraduate: Bowling Green State University </li> 
            	<ul>
                	<li>Current GPA: 3.952</li>
                	<li>Major: Mathematics with a specialization in Data Science </li>
                	<li>Minor: Computer Science</li>
                
                	<li>Graduation Date <font size = "1">(Hopefully) </font size = "1">: December 2021 </li>
                </ul>
            <li>Graduate: Bowling Green State University </li>
            	<ul>
                	<li>Masters Degree: Data Science</li>
                    <li>Graduation Date <font size = "1">(Hopefully) </font size = "1">: December 2022 </li>
                </ul>
        </ul>
			</div>
            
		<h2>
        Employment Information
        </h2>
        	<div>
        	<ul>
            	<li>Kentucky Fried Chicken</li>
                <ul>
                	<li>Team Member</li>
                    <li>April 2017-July 2018</li>
                </ul>
                <li>Kentucky Fried Chicken</li>
                <ul>
                	<li>Shift Supervisor</li>
                    <li>July 2018-NOW</li>
                </ul>
                <li>Progressive</li>
                <ul>
                	<li>Data Analyst Intern</li>
                    <li>Summer 2021<font size = "1">(Hopefully) </font size = "1"> </li>
                </ul>
            </ul>
           </div>

</body>
</html>
